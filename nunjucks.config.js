const moment = require('moment-timezone')
const fetch = require('node-fetch')
const html2plaintext = require('html2plaintext')

module.exports = {
  root: './src',
  data: getData
}

async function getData (_pathInfo) {
  let api = new ApiClient('https://directus-renee-u21722.vm.elestio.app', 'Europe/Zurich', 6)
  let events = await api.getEvents()
  let homepage = await api.getHomepage()

  return {
    events,
    homepage,
  }
}

class ApiClient {
  url
  timeZone
  dayOffset

  constructor (url, timeZone, dayOffset) {
    this.url = url
    this.timeZone = timeZone
    this.dayOffset = dayOffset
  }

  async getEvents () {
    let dateMin = moment().tz(this.timeZone)
      .subtract(this.dayOffset, 'hours')
      .set({'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0})
      .add(this.dayOffset, 'hours')
      .toISOString()
    let response = await this._get(`/items/events?filter[start][_gte]=${dateMin}&sort=start`)

    return response.data.map((event) => {
      let start = moment.tz(event['start'], this.timeZone)
      let start_text;
      if (start.format('H') < this.dayOffset) {
        start.subtract(1, 'day')
        start_text = start.format('dd [night], D MMM H:mm');
      } else {
        start_text = start.format('dd, D MMM H:mm');
      }
      let end = start.clone().add(1, 'day').startOf('day').add(2, 'hours');
      return {
        start_text,
        start_iso: start.toISOString(true),
        end_iso: end.toISOString(true),
        title_html: event['title'],
        title_text: html2plaintext(event['title']),
      }
    })
  }

  async getHomepage () {
    let response = await this._get('/items/website_reneech?single=1')
    return {
      html_title: response.data['html_title'],
      html_description: response.data['html_description'],
      html_keywords: response.data['html_keywords'],
      opening_hours: response.data['opening_hours'],
      menu: response.data['menu'],
      imprint: response.data['imprint'],
      google_tag_id: 'G-FHRCXN3GYR',
    }
  }

  async _get (path) {
    let url = this.url + path
    let response = await fetch(url, {method: 'get'})
    let response_json = await response.json();
    let response_errors = response_json['errors']
    if (response_errors) {
      throw new Error(`API error: ${JSON.stringify(response_errors)}`)
    }
    return response_json
  }
}
